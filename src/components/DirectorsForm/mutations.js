import {gql} from 'apollo-boost';

export const addDirectorMutation = gql`
    mutation addDirector($name: String!, $age: String!) {
        addDirector(name: $name, age: $age) {
            name
            age
        }
    }
`;

export const updatedDirectorMutation = gql`
  mutation updateDirector($id: ID, $name: String!, $age: String!) {
    updateDirector(id: $id, name: $name, age: $age) {
      name
    }
  }
`;