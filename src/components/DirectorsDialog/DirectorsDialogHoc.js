
import { compose } from 'recompose';
import {graphql} from 'react-apollo';

import {deleteDirectoryMutation} from './mutations';
import {directorsQuery} from '../DirectorsTable/queries';

const withGraphQLDelete = graphql(deleteDirectoryMutation, {
    props: ({mutate}) => ({
        deleteDirectory: id => mutate({
            variables: id,
            refetchQueries: [{
                query: directorsQuery,
                variables: {name: ''}
            }],
        })
    })
})

export default compose(withGraphQLDelete);

