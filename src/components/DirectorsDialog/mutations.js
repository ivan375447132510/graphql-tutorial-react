import {gql} from 'apollo-boost';

export const deleteDirectoryMutation = gql`
    mutation deleteDirectory($id: ID!) {
        deleteDirectory(id: $id ) {
            id
        }
    }
`;